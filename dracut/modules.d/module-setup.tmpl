#!/bin/bash
# -*- mode: sh; indent-tabs-mode: nil; sh-basic-offset: 4; -*-
# vim: et sts=4 sw=4

#  SPDX-License-Identifier: LGPL-2.1+
#
#  Copyright © 2019-2021 Collabora Ltd.
#  Copyright © 2019-2021 Valve Corporation.
#
#  This file is part of steamos-customizations.
#
#  steamos-customizations is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public License as
#  published by the Free Software Foundation; either version 2.1 of the License,
#  or (at your option) any later version.

@STEAMOS_BINARIES@

# called by dracut
check() {
    # basic commands
    require_binaries \
        basename bash dd dirname iconv rmdir sed tr udevadm \
        || return 1

    # for factory reset at shutdown
    require_binaries \
        mkfs.ext4 tune2fs \
        || return 1

    # steamos helpers for partsets
    require_binaries \
        lsblk sort \
        || return 1

    # systemd helpers for dm-verity
    require_binaries \
        $systemdutildir/systemd-veritysetup \
        $systemdutildir/system-generators/systemd-veritysetup-generator \
        || return 1

    return 0
}

# called by dracut
depends() {
    echo crypt
    return 0
}

# called by dracut
installkernel() {
    hostonly='' instmods ext4 overlay dm-verity
    # The ESP (hence vfat) requires various modules to be mounted, see:
    # - https://wiki.gentoo.org/wiki/EFI_System_Partition
    # - https://wiki.gentoo.org/wiki/FAT
    hostonly='' instmods fat vfat nls_ascii nls_cp437 nls_iso8859-1
}

# called by dracut
install() {
    inst_dir /mnt
    inst_multiple "${STEAMOS_BINARIES[@]}"

    inst_multiple \
        dd tr \
        iconv /usr/lib/gconv/UTF-16.so /usr/lib/gconv/gconv-modules \
        $systemdutildir/systemd-veritysetup \
        $systemdutildir/system-generators/systemd-veritysetup-generator

    inst_hook cmdline   90 "$moddir/steamos-parse-loaders.sh"
    inst_hook pre-mount 90 "$moddir/steamos-udev-rules.sh"
    inst_hook mount     90 "$moddir/steamos-root.sh"
    inst_hook pre-pivot 88 "$moddir/steamos-factory-reset.sh"
    inst_hook pre-pivot 89 "$moddir/steamos-var.sh"
    inst_hook pre-pivot 90 "$moddir/steamos-etc-overlay.sh"
    inst_hook pre-pivot 90 "$moddir/steamos-var-lib-modules.sh"
    inst_hook pre-pivot 99 "$moddir/steamos-emergency.sh"
}
