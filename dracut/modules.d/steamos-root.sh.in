#!/bin/bash
# -*- mode: sh; indent-tabs-mode: nil; sh-basic-offset: 4; -*-
# vim: et sts=4 sw=4

#  SPDX-License-Identifier: LGPL-2.1+
#
#  Copyright © 2021 Collabora Ltd.
#  Copyright © 2021 Valve Corporation.
#
#  This file is part of steamos-customizations.
#
#  steamos-customizations is free software; you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published
#  by the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.

UDEV_SYMLINKS_RELDIR=@udev_symlinks_reldir@

. /lib/dracut-lib.sh

mount_root() {
    declare -a opts

    if [[ ${rflags} ]]; then
        opts+=(-o ${rflags})
    fi

    if [[ "${fstype:-auto}" != auto ]]; then
        opts+=(-t "$fstype")
    fi

    # Mount the / mountpoint

    info "Mounting /dev/${UDEV_SYMLINKS_RELDIR}/self/rootfs with ${opts[*]}"
    mount "${opts[@]}" "/dev/${UDEV_SYMLINKS_RELDIR}/self/rootfs" "$NEWROOT" 2>&1 | vinfo
    if ismounted "$NEWROOT"; then
        return
    fi

    warn "Mounting /dev/${UDEV_SYMLINKS_RELDIR} failed!"
    warn "*** Dropping you to a shell; the system will continue"
    warn "*** when you leave the shell."
    emergency_shell
}

if ismounted "$NEWROOT"; then
    return
fi
mount_root
